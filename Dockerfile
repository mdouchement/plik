# build stage
#
# Await https://github.com/mattn/go-sqlite3/pull/1177 to be updated in Plik
FROM golang:1.21-alpine3.18 as build-env
MAINTAINER mdouchement

# Set the locale
ENV LANG c.UTF-8

# Install build dependencies
RUN apk upgrade
RUN apk add --update --no-cache \
  ca-certificates \
  bash \
  git \
  build-base \
  nodejs \
  npm

RUN git clone https://github.com/root-gg/plik.git /go/src/github.com/root-gg/plik
WORKDIR /go/src/github.com/root-gg/plik
RUN git checkout 1.3.8


RUN make frontend
RUN make server

# final stage
FROM alpine
MAINTAINER mdouchement

RUN apk add --update --no-cache shadow ca-certificates

ENV UID=1003
ENV GID=1003
RUN groupadd -g "${GID}" plik
RUN useradd -d /opt/plik -u "${UID}" -g "${GID}" -m plik

COPY --from=build-env /go/src/github.com/root-gg/plik/webapp/dist         /opt/plik/webapp/dist
COPY --from=build-env /go/src/github.com/root-gg/plik/server/plikd        /opt/plik/server/plikd
COPY --from=build-env /go/src/github.com/root-gg/plik/server/plikd.cfg    /opt/plik/plikd.cfg

RUN chown -R ${UID}:${GID} /opt/plik

USER plik
WORKDIR /opt/plik/server

EXPOSE 8080
CMD ["/opt/plik/server/plikd"]
